<?php
/* Template Name: Apartment Content */

get_header(); ?>
<?php renderFirstFrame(); ?>
<?php renderWelcome(); ?>
<?php renderApartments(); ?>
<?php get_footer(); ?>

