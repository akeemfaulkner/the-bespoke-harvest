var start = new google.maps.LatLng(42.291609, 11.6405891);
var end = new google.maps.LatLng(42.4224989, 12.0753709);



var styles = [
  {
    featureType: "water",
    elementType: "all",
    stylers: [
      {color: '#e1e1e1'},
      {visibility: "on"}
    ]
  }, {
    featureType: "road",
    stylers: [
      {"visibility": "off"}
    ]
  }, {
    featureType: "transit",
    stylers: [
      {"visibility": "off"}
    ]
  }, {
    featureType: "administrative",
    stylers: [
      {"visibility": "off"}
    ]
  }, {
    featureType: "landscape",
    elementType: "all",
    stylers: [
      {"color": "#e1e1e1"}
    ]
  }, {
    featureType: "poi",
    //elementType: "labels",
    stylers: [{
      visibility: "off"
    }]
  },
  {
    elementType: "labels",
    stylers: [
      {"visibility": "off"}
    ]
  }
];

function initialize() {
  var map = new google.maps.Map(document.getElementById("map"), {
    center: start,
    zoom: 8,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    disableDefaultUI: true,
    styles: styles,
    scrollwheel: false,
    navigationControl: false,
    mapTypeControl: false,
    scaleControl: false,
    draggable: false,
    disableDoubleClickZoom: true
  })

  var whiteCircle = {
    path: google.maps.SymbolPath.CIRCLE,
    fillOpacity: 1.0,
    fillColor: "white",
    strokeOpacity: 1.0,
    strokeColor: "white",
    strokeWeight: 1.0,
    scale: 5.0
  };

  new google.maps.Marker({
    icon: whiteCircle,
    position: start
  }).setMap(map);

  new google.maps.Marker({
    icon: whiteCircle,
    position: end
  }).setMap(map);


  var layer = new google.maps.FusionTablesLayer({
    query: {
      select: '\'Id\'',
      from: '1ooVyXMEnEBkDaKAuzTypdFisXGZB87-n-hRwUWM'

    },
    styles: [{
      polygonOptions: {
        fillColor: '#e1e1e1',
        fillOpacity: 1.0
      }
    }, {
      where: 'Id = \'Italy\'',
      polygonOptions: {
        fillColor: '#959595',
        fillOpacity: 1.0
      }
    }],
    options: {
      suppressInfoWindows: true
    }
  });
  layer.setMap(map);
  getDirections(map);
}


function autoRefresh(map, pathCoords) {
  var i, route, marker;

  route = new google.maps.Polyline({
    path: [],
    geodesic: true,
    strokeColor: '#fac005',
    strokeOpacity: 1.0,
    strokeWeight: 4,
    editable: false,
    map: map
  });


  for (i = 0; i < pathCoords.length; i++) {
    setTimeout(function (coords) {
      route.getPath().push(coords);
    }, 5 * i, pathCoords[i]);
  }
}

function getDirections(map) {
  var directionsService = new google.maps.DirectionsService();

  var request = {
    origin: start,
    destination: end,
    travelMode: google.maps.TravelMode.DRIVING
  };
  directionsService.route(request, function (result, status) {
    if (status === google.maps.DirectionsStatus.OK) {
      autoRefresh(map, result.routes[0].overview_path);
    }
  });
}

google.maps.event.addDomListener(window, 'load', initialize);
