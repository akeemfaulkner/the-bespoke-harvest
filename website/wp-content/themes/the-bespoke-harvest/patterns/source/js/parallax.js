var getElemDistance = function (elem) {
  var location = 0;
  if (elem.offsetParent) {
    do {
      location += elem.offsetTop;
      elem = elem.offsetParent;
    } while (elem);
  }
  return location >= 0 ? location : 0;
};

var last_known_scroll_position = 0;
var ticking = false;

var parallaxElements = [].slice.apply(document.querySelectorAll('.parallax'));
function doSomething(scroll_pos) {
  parallaxElements.forEach(function (element) {
    var elemDistance = getElemDistance(element);
    var boundingClientRect = element.parentElement.getBoundingClientRect();
    var height = boundingClientRect.bottom - boundingClientRect.top;
    if (elemDistance <= scroll_pos) {
      element.style.transform = 'translateY(' + (scroll_pos - elemDistance) + 'px)'
    }
    if (elemDistance + height <= scroll_pos || elemDistance > scroll_pos) {
      element.style.transform = 'translateY(0px)'
    }

  });

}

function detectmob() {
  if( navigator.userAgent.match(/Android/i)
    || navigator.userAgent.match(/webOS/i)
    || navigator.userAgent.match(/iPhone/i)
    || navigator.userAgent.match(/iPad/i)
    || navigator.userAgent.match(/iPod/i)
    || navigator.userAgent.match(/BlackBerry/i)
    || navigator.userAgent.match(/Windows Phone/i)
  ){
    return true;
  }
  else {
    return false;
  }
}
if (!detectmob()) {
  window.addEventListener('scroll', function (e) {
    last_known_scroll_position = window.pageYOffset;
    if (!ticking) {
      window.requestAnimationFrame(function () {
        doSomething(last_known_scroll_position);
        ticking = false;
      });
    }
    ticking = true;
  });
}

