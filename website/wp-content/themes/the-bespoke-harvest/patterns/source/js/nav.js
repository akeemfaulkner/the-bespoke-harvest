(function () {
  var $toggleButton = document.querySelector('.nav-toggle');
  var navActiveClassName = 'nav--active';
  var $nav = document.querySelector('.nav');

  $toggleButton.addEventListener('click', function (event) {
    var navClassNames = $nav.className;

    event.preventDefault();

    if (navClassNames.indexOf(navActiveClassName) === -1) {
      $nav.className += ' ' + navActiveClassName;
    } else {
      $nav.className = $nav.className.replace(navActiveClassName, '');
    }
  }, false);
})();
