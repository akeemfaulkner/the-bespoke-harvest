<?php
function tbhOrchardShortcode()
{
    $orchard_options = get_option('orchard_option', array('coords' => json_encode([])));
    $rows = json_decode($orchard_options['coords']);
    $posts = postQueryToMapById(array('post_type' => 'tree', 'post_status' => 'publish'));
    $counter = 1;

    ob_start();
    ?>
    <div class="orchard">
        <div class="orchard__header">
            <h1 class="header">This is our field</h1>

        </div>
        <div class="row row--wrap orchard__row">

            <?php foreach ($rows as $row) : ?>
                <?php foreach ($row as $index => $id): ?>

                    <?php if (empty($posts[$id])): ?>
                        <div class="col-20"></div>
                    <?php else: ?>
                        <?php
                        $isAvailable = get_field('is_available', $id);
                        ?>
                        <div class="col-20 tree" tabindex="0">
                            <span class="tree-number"><?= str_pad($counter++, 3, "00", STR_PAD_LEFT); ?></span>
                            <img src="<?= get_template_directory_uri() ?>/images/tree.png" alt="" class="tree-icon">

                            <div class="tree-name">
                                <div class="italic-small-text">
                                    <?= $posts[$id]->post_title ?>
                                </div>

                            </div>
                            <div class="tree-availability <?= $isAvailable ? '' : 'tree-availability--unavailable' ?>">
                                <?= $isAvailable ? 'AVAILABLE' : 'UNAVAILABLE' ?>
                            </div>
                            <form class="tree__ctas" method="post">
                                <div class="tree__cta">
                                    <a class="hollow-button" href="<?= get_permalink($id) ?>">Learn More</a>
                                </div>
                                <div class="tree__cta">
                                    <button class="hollow-button" name="<?= ShoppingCart::$Options['bookNow'] ?>"
                                            value="<?= get_field('sku', $id) ?>">Book Now
                                    </button>
                                </div>
                            </form>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endforeach; ?>

        </div>
    </div>
    <?php
    $component = ob_get_contents();
    ob_end_clean();
    return $component;
}

add_shortcode('tbhOrchard', 'tbhOrchardShortcode');