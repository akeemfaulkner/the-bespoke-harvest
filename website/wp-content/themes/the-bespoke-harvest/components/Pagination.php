<?php
function tbhPaginationShortcode($atts)
{

    $values = shortcode_atts(array(
        'targets' => array()
    ), $atts);

    $targets = json_decode(base64_decode($values['targets']));


    ob_start();

    ?>

    <div class="pagination">
        <?php foreach ((array)$targets as $target) : ?>
            <div class="pagination__point pagination__point--inactive" data-pagination-target="<?=$target ?>">
            </div>
        <?php endforeach; ?>
    </div>
    <?php
    $component = ob_get_contents();
    ob_end_clean();
    return $component;
}

add_shortcode('tbhPagination', 'tbhPaginationShortcode');