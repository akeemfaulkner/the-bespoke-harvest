<?php
function tbhInstagramShortcode($atts)
{
    $values = shortcode_atts(array(
    ), $atts);
    ob_start();
    ?>
    <div class="instagram-feed">
        <div class="instagram-feed__header">
            <h1 class="header">APPARTAMENT "riva del lago"</h1>

        </div>
        <ul class="grid-6-2">
            <li class="grid-6-2__item">
                <a href="#">
                    <div class="image-square">
                        <img src="<?= get_template_directory_uri() ?>/images/product.png" alt="">
                    </div>
                </a>

            </li>
            <li class="grid-6-2__item">
                <a href="#">
                    <div class="image-square">
                        <img src="<?= get_template_directory_uri() ?>/images/product.png" alt="">
                    </div>
                </a>

            </li>
            <li class="grid-6-2__item">
                <a href="#">
                    <div class="image-square">
                        <img src="<?= get_template_directory_uri() ?>/images/product.png" alt="">
                    </div>
                </a>

            </li>
            <li class="grid-6-2__item">
                <a href="#">
                    <div class="image-square">
                        <img src="<?= get_template_directory_uri() ?>/images/product.png" alt="">
                    </div>
                </a>

            </li>
            <li class="grid-6-2__item">
                <a href="#">
                    <div class="image-square">
                        <img src="<?= get_template_directory_uri() ?>/images/product.png" alt="">
                    </div>
                </a>

            </li>
            <li class="grid-6-2__item">
                <a href="#">
                    <div class="image-square">
                        <img src="<?= get_template_directory_uri() ?>/images/product.png" alt="">
                    </div>
                </a>

            </li>
            <li class="grid-6-2__item">
                <a href="#">
                    <div class="image-square">
                        <img src="<?= get_template_directory_uri() ?>/images/product.png" alt="">
                    </div>
                </a>

            </li>
            <li class="grid-6-2__item">
                <a href="#">
                    <div class="image-square">
                        <img src="<?= get_template_directory_uri() ?>/images/product.png" alt="">
                    </div>
                </a>

            </li>
            <li class="grid-6-2__item">
                <a href="#">
                    <div class="image-square">
                        <img src="<?= get_template_directory_uri() ?>/images/product.png" alt="">
                    </div>
                </a>

            </li>
            <li class="grid-6-2__item">
                <a href="#">
                    <div class="image-square">
                        <img src="<?= get_template_directory_uri() ?>/images/product.png" alt="">
                    </div>
                </a>

            </li>
            <li class="grid-6-2__item">
                <a href="#">
                    <div class="image-square">
                        <img src="<?= get_template_directory_uri() ?>/images/product.png" alt="">
                    </div>
                </a>

            </li>
            <li class="grid-6-2__item">
                <a href="#">
                    <div class="image-square">
                        <img src="<?= get_template_directory_uri() ?>/images/product.png" alt="">
                    </div>
                </a>

            </li>
        </ul>

        <div class="instagram-feed__cta">
            <button class="hollow-button">Learn More</button>

        </div>

    </div>
    <?php
    $component = ob_get_contents();
    ob_end_clean();
    return $component;
}

add_shortcode('tbhInstagram', 'tbhInstagramShortcode');