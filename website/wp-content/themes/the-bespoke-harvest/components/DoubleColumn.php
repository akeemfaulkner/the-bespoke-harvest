<?php
function tbhDoubleColumnShortcode($atts)
{
    $values = shortcode_atts(array(
        'column-1' => '',
        'column-2' => '',
    ), $atts);

    ob_start();
    ?>


        <div class="row row--center row--wrap col-2-layout">
            <div class="col col-50">
                <?= decode_shortcode_data($values['column-1'])?>
            </div>
            <div class="col col-50">
                <?= decode_shortcode_data($values['column-2'])?>
            </div>
        </div>


    <?php
    $component = ob_get_contents();
    ob_end_clean();
    return $component;
}

add_shortcode('tbhDoubleColumn', 'tbhDoubleColumnShortcode');