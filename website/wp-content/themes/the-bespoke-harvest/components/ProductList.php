<?php
function tbhProductListShortcode($atts)
{
    $values = shortcode_atts(array(
    ), $atts);
    $posts = postQueryToMapById(array('post_type' => 'olive_oil', 'post_status' => 'publish'));
    ob_start();
    ?>

    <section class="section-scroller">
        <div class="content" >
            <div class="content__header">
                <div class="serif-header">DISCOVER at the speed of life</div>

            </div>
            <div class="product-list">
                <?php foreach ($posts as $post) : ?>
                    <?= do_shortcode('[tbhProductItem 
                                                                sku="'.encode_shortcode_data(get_field('sku', $post->ID)) .'"
                                                                image="' . encode_shortcode_data(get_field('thumbnail', $post->ID)) . '" 
                                                                excerpt="' . encode_shortcode_data(get_field('excerpt', $post->ID)) . '" 
                                                                link="' . encode_shortcode_data(get_permalink($post->ID)) . '" 
                                                                ]') ?>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <?php
    $component = ob_get_contents();
    ob_end_clean();
    return $component;
}

add_shortcode('tbhProductList', 'tbhProductListShortcode');