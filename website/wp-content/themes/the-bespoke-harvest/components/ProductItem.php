<?php
function tbhProductItemShortcode($atts)
{

    $values = shortcode_atts(array(
        'image' => '',
        'excerpt' => '',
        'sku' => '',
        'link' => '',
    ), $atts);
    $sku = decode_shortcode_data($values['sku']);
    ob_start();
    ?>
    <div class="product" tabindex="0">
        <div class="card">
            <div class="card-content">
                <div class="card-content__header">
                    <h1 class="italic-header">Few steps from the lake</h1>
                </div>
                <div class="card-image">
                    <div class="image-square">
                        <img src="<?= decode_shortcode_data($values['image']) ?>" alt="">
                    </div>

                </div>
                <div class="card-content__text">
                    <?= decode_shortcode_data($values['excerpt']) ?>
                </div>
            </div>
            <form class="product__ctas" method="post">
                <div class="product__cta">
                    <a class="hollow-button" href="<?= decode_shortcode_data($values['link']) ?>">Learn More</a>
                </div>
                <div class="product__cta">
                    <button class="hollow-button"  value="<?= $sku ?>" name="<?=ShoppingCart::$Options['addToCart'] ?>">Add to Cart</button>
                </div>
            </form>
        </div>
    </div>
    <?php
    $component = ob_get_contents();
    ob_end_clean();
    return $component;
}

add_shortcode('tbhProductItem', 'tbhProductItemShortcode');