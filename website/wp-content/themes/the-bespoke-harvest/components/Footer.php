<?php
function tbhFooterShortcode($atts)
{
    $values = shortcode_atts(array(

    ), $atts);

    ob_start();
    ?>
    <div class="footer">
        <div class="footer__logo">
            <img src="<?= get_template_directory_uri() ?>/images/logo-inverted.png" alt="">

        </div>

        <div class="footer__social-icons">


            <span class="facebook-icon"></span>


            <span class="twitter-icon"></span>


            <span class="instagram-icon"></span>


            <span class="pinterest-icon"></span>


            <span class="linkedin-icon"></span>


            <span class="youtube-icon"></span>


            <span class="googleplus-icon"></span>

        </div>
    </div>
    <?php
    $component = ob_get_contents();
    ob_end_clean();
    return $component;
}

add_shortcode('tbhFooter', 'tbhFooterShortcode');