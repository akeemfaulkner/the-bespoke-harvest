<?php
function tbhSingleColumnShortcode($atts)
{
    $values = shortcode_atts(array(
        'column' => ''
    ), $atts);

    ob_start();
    ?>

    <div class="row row--center col-1-layout">
        <div class="col-60 col">
            <?= decode_shortcode_data($values['column']) ?>
        </div>
    </div>

    <?php
    $component = ob_get_contents();
    ob_end_clean();
    return $component;
}

add_shortcode('tbhSingleColumn', 'tbhSingleColumnShortcode');