<?php
function tbhWelcomeShortcode($atts)
{
    $values = shortcode_atts(array(
        'content' => ''
    ), $atts);
    ob_start();
    ?>
    <div class="hero welcome">
        <div class="hero-content welcome__content">
            <div class="italic-header"><?= decode_shortcode_data($values['content']) ?></div>
        </div>
        <div class="underline"></div>
    </div>
    <?php
    $component = ob_get_contents();
    ob_end_clean();
    return $component;
}

add_shortcode('tbhWelcome', 'tbhWelcomeShortcode');