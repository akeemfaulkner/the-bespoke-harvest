<?php
function tbhProductShortcode($atts)
{
    $values = shortcode_atts(array(
        'image' => '',
        'description' => '',
        'hide-cart' => false,
        'year' => '',
        'sku' => '',
        'quality' => '',
        'geolocation' => '',
    ), $atts);
    $geolocation = decode_shortcode_data($values['geolocation']);
    $quality = decode_shortcode_data($values['quality']);
    $year = decode_shortcode_data($values['year']);
    $sku = decode_shortcode_data($values['sku']);
    $hideCart = decode_shortcode_data($values['hide-cart']);
    ob_start();
    ?>
    <div class="content content--padded">
        <div class="content__header">
            <h1 class="italic-header"><?= the_title(); ?></h1>

        </div>
        <div class="product__single">
            <div class="product-inner">
                <div class="product__image">
                    <div class="image-square">
                        <img src="<?= decode_shortcode_data($values['image']) ?>" alt="">
                    </div>

                </div>
                <div class="product__content">
                    <div class="product__content-description">
                        <?= decode_shortcode_data($values['description']) ?>
                        <form class="product__single-ctas" method="post" action="" <?= $hideCart ? 'style="justify-content: center;"' : '' ?>>
                            <button class="hollow-button" value="<?= $sku ?>"
                                    name="<?= ShoppingCart::$Options['bookNow'] ?>">
                                Book Now
                            </button>
                            <?php if (!$hideCart) : ?>
                                <button class="hollow-button" value="<?= $sku ?>"
                                        name="<?= ShoppingCart::$Options['addToCart'] ?>">Add to Cart
                                </button>
                            <?php endif; ?>
                        </form>
                    </div>
                    <div class="product__content-meta">
                        <div class="meta">
                            <?php if ($year) : ?>
                                <div class="meta-group">
                                    <div class="bold-text">
                                        Year
                                    </div>

                                    <div class="small-text">
                                        <?= $year ?>
                                    </div>

                                </div>
                            <?php endif; ?>
                            <?php if ($quality) : ?>
                                <div class="meta-group">
                                    <div class="bold-text">
                                        Quality
                                    </div>

                                    <div class="small-text">
                                        <?= $quality ?>
                                    </div>

                                </div>
                            <?php endif; ?>
                            <?php if ($geolocation) : ?>
                                <div class="meta-group">
                                    <div class="bold-text">
                                        Geolocation
                                    </div>

                                    <div class="small-text">
                                        <?= $geolocation->address ?>
                                    </div>

                                </div>
                            <?php endif; ?>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    $component = ob_get_contents();
    ob_end_clean();
    return $component;
}

add_shortcode('tbhProduct', 'tbhProductShortcode');