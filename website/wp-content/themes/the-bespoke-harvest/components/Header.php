<?php
function tbhHeaderShortcode($atts)
{
    $values = shortcode_atts(array(
        'items' => array(),
        'cart' => array()
    ), $atts);


    $cart = decode_shortcode_data($values['cart']);
    $menu_items = decode_shortcode_data($values['items']);
    $logo_url = get_template_directory_uri() . "/images/logo.png";
    $cart_url = get_template_directory_uri() . "/images/cart.png";
    $logo_alt = "The Bespoke Harvest Logo";
    $cart_alt = "Shopping cart";
    $cartCount = count($cart);
    ob_start();
    ?>
    <div class="nav">

        <a href="/" class="nav__logo-link">
            <img src="<?= $logo_url ?>" alt="<?= $logo_alt ?>" class="logo">
        </a>
        <button class="nav-toggle"></button>
        <ul class="nav-list">

            <?php
            $menu_list = '';
            foreach ((array)$menu_items as $key => $menu_item) {
                $title = $menu_item->title;
                $url = $menu_item->url;
                $active_class = get_permalink() === $url ? 'nav__item--active' : '';
                if ($title == 'logo') {
                    $menu_list .=
                        '<li class="nav__item nav__item--center">' .
                        '<a href="/" class="nav__item-link">' .
                        '<img src="' . $logo_url . '" alt="' . $logo_alt . '" class="logo">' .
                        '</a>' .
                        '</li>';

                } else {
                    $menu_list .= "\t\t\t\t\t" . '<li class="nav__item ' . $active_class . '"><a href="' . $url . '" class="nav__item-link"><div class="uppercase-text">' . $title . '</div></a></li>' . "\n";
                }
            }

            echo $menu_list;

            ?>

        </ul>

        <ul class="nav-list--fixed">

            <?php
            $menu_list = '';
            foreach ((array)$menu_items as $key => $menu_item) {
                $title = $menu_item->title;
                $url = $menu_item->url;
                $active_class = get_permalink() === $url ? 'nav__item--active' : '';
                if ($title == 'logo') {
                    $menu_list .=
                        '<li class="nav__item nav__item--center">' .
                        '<a href="/" class="nav__item-link">' .
                        '<img src="' . $logo_url . '" alt="' . $logo_alt . '" class="logo">' .
                        '</a>' .
                        '</li>';

                } else {
                    $menu_list .= "\t\t\t\t\t" . '<li class="nav__item ' . $active_class . '"><a href="' . $url . '" class="nav__item-link"><div class="uppercase-text">' . $title . '</div></a></li>' . "\n";
                }
            }

            echo $menu_list;

            ?>

        </ul>
        <a href="/checkout" class="nav__cart-link">

                <span class="nav__cart-count">
                <?= $cartCount ?>
                </span>
            <img src="<?= $cart_url ?>" alt="<?= $cart_alt ?>" class="logo">
        </a>
    </div>
    <?php
    $component = ob_get_contents();
    ob_end_clean();
    return $component;
}

add_shortcode('tbhHeader', 'tbhHeaderShortcode');