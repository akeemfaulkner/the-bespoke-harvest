angular.module('orchard-admin', [])
    .controller('rowCtrl', function ($scope) {
        $scope.rows = [];
        $scope.$watch('formData', function () {
            let parsedData = parseFormData($scope.formData);
            if(!$scope.rows.length && parsedData.length) {
                $scope.rows = parsedData;
            }
        });


        $scope.addRow = function () {
            $scope.rows.push([
                '',
                '',
                '',
                '',
                '',
            ]);
        };
        
        $scope.updateCoordinates = function () {
            $scope.formData = JSON.stringify($scope.rows);
        };

        function parseFormData(formData){
            try {
                return JSON.parse(formData);
            } catch(e) {
                return [];
            }
        }
    });