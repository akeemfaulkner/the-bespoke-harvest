<?php

class MySettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;
    private $trees;

    /**
     * Start up
     */
    public function __construct()
    {
        $query = new WP_Query(array('post_type' => 'tree'));
        $this->trees = $query->posts;
        add_action('admin_menu', array($this, 'add_plugin_page'));
        add_action('admin_init', array($this, 'page_init'));
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Settings Admin',
            'Orchard Settings',
            'manage_options',
            'my-orchard-admin',
            array($this, 'create_admin_page')

        );

    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option('orchard_option');
        $options = $this->getTreeOptions();

        ?>
        <div class="wrap" ng-app="orchard-admin">
            <form method="post" action="options.php" ng-controller="rowCtrl">
                <?php
                // This prints out all  setting fields
                settings_fields('orchard_group');
                do_settings_sections('my-setting-admin');

                $value = isset($this->options['coords']) ? esc_attr($this->options['coords']) : '';
                printf(
                    '<input style="display: none;" type="text" id="coords" name="orchard_option[coords]" value="%s" ng-model="formData" ng-init="formData = \'%s\'"/>',
                    $value,
                    $value
                );

                ?>

                <div class="row clearfix">
                    <div class="col-sm-12">
                        <button class="btn  btn-primary pull-right" type="button" ng-click="addRow()">Add Row</button>
                    </div>
                </div>
                <br>
                <table class="table-bordered table">

                    <tr ng-repeat="row in rows track by $index">
                        <td>
                            <select ng-model="rows[$index][0]" ng-change="updateCoordinates()">
                                <?= $options ?>
                            </select>
                        </td>
                        <td>
                            <select ng-model="rows[$index][1]" ng-change="updateCoordinates()">
                                <?= $options ?>
                            </select>
                        </td>
                        <td>
                            <select ng-model="rows[$index][2]" ng-change="updateCoordinates()">
                                <?= $options ?>
                            </select>
                        </td>
                        <td>
                            <select ng-model="rows[$index][3]" ng-change="updateCoordinates()">
                                <?= $options ?>
                            </select>
                        </td>
                        <td>
                            <select ng-model="rows[$index][4]" ng-change="updateCoordinates()">
                                <?= $options ?>
                            </select>
                        </td>
                    </tr>
                </table>
                <?php submit_button(); ?>
            </form>
        </div>
        <?php

    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize($input)
    {
        $new_input = array();
        if (isset($input['coords']))
            $new_input['coords'] = json_encode($input['coords']);


        return $new_input;
    }


    /**
     * Register and add settings
     */
    public function page_init()
    {
        register_setting(
            'orchard_group', // Option group
            'orchard_option', // Option name
            array($this) // Sanitize
        );

        add_settings_section(
            'setting_section_coord', // ID
            'My Orchard Layout', // Title
            array($this, 'print_section_info'), // Callback
            'my-setting-admin' // Page
        );

    }

    /**
     * Print the Section text
     */
    public function print_section_info()
    {
        print '';
    }


    /**
     * Get options for the select inputs
     */
    public function getTreeOptions()
    {
        $options = array_map(function ($tree) {
            return '<option value="' . $tree->ID . '">' . $tree->post_title . '</option>';
        }, $this->trees);

        return '<option value="">Select Tree...</option>' . join('', $options);
    }

}

if (is_admin())
    $my_settings_page = new MySettingsPage();