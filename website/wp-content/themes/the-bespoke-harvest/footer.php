<?php
/**
 * The template for displaying the footer
 *
 *
 * @subpackage the-bespoke-harvest
 */
?>
<section class="section-scroller">
    <?= do_shortcode('[tbhFooter]') ?>
</section>
<?php wp_footer(); ?>
</body>
</html>
