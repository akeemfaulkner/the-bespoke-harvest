<?php
/* Template Name: Single Column Content */

get_header(); ?>
<?php renderFirstFrame(); ?>

<?php renderWelcome(); ?>

<section class="section-scroller">
    <?= do_shortcode('[tbhSingleColumn]') ?>
</section>
<section class="section-scroller">
    <div class="content">

        <div class="content__header">
            <div class="leaf-header">OUR STORY</div>

        </div>

        <?= renderSingleColumn(); ?>

    </div>
</section>
<?php renderHeroes(); ?>
<section class="section-scroller">
    <?= do_shortcode('[tbhInstagram]') ?>
</section>
<?php get_footer(); ?>

