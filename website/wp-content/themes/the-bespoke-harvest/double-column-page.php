<?php
/* Template Name: Double Column Content */
?>
<?php get_header(); ?>


<?php renderFirstFrame(); ?>

<?php renderWelcome(); ?>
<section class="section-scroller">
    <div class="content">
        <div class="content__header">
            <div class="serif-header">DISCOVER<br>at the speed of life</div>

        </div>

        <?= renderDoubleColumn() ?>

    </div>
</section>
<?php renderHeroes(); ?>
<section class="section-scroller">
    <?= do_shortcode('[tbhInstagram]') ?>
</section>
<?php get_footer(); ?>

