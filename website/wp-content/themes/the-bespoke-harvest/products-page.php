<?php
/* Template Name: Products */

get_header(); ?>
<section class="section-scroller">
    <?= do_shortcode('[tbhFrame]') ?>
</section>
<section class="section-scroller">
    <?= do_shortcode('[tbhWelcome]') ?>
</section>
<?= do_shortcode('[tbhProductList]')?>
<?= do_shortcode('[tbhHero]')?>
<?= do_shortcode('[tbhHero]')?>
<?php get_footer(); ?>

