<?php
/**
 * The template for displaying the header
 *
 * @subpackage the-bespoke-harvest
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <!--[if lt IE 9]>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
    <![endif]-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCo3e_2xea4jd7wMQ2c0IkQKRQ3NH3aZmY&libraries=geometry"></script>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<section class="section-scroller">
    <?php if (function_exists(clean_custom_menus())) clean_custom_menus(); ?>
</section>

