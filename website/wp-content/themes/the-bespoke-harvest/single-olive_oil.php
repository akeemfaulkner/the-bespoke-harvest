<?php get_header(); ?>
<?php renderFirstFrame(); ?>
<section class="section-scroller">
    <?= do_shortcode('[tbhProduct
                    sku="' . encode_shortcode_data(get_field('sku')) . '"
                    geolocation="' . encode_shortcode_data(get_field('geolocation')) . '"
                    image="' . encode_shortcode_data(get_field('image')) . '"
                    description="' . encode_shortcode_data(get_field('description')) . '"
                    year="' . encode_shortcode_data(get_field('year')) . '"
                    quality="' . encode_shortcode_data(get_field('quality')) . '"
                    ]') ?>
</section>
<?php get_footer(); ?>
