<?php

class ShoppingCart
{
    public static $Options = array(
        'addToCart' => 'add_to_cart',
        'removeFromCart' => 'remove_from_cart',
        'setItemAmount' => 'set_item_amount',
        'bookNow' => 'book_now',
        'getCart' => 'get_cart',
    );

    private $publicHandlers = array();

    function __construct()
    {
        $this->publicHandlers = array_flip(ShoppingCart::$Options);
    }

}

$Cart = new ShoppingCart();

