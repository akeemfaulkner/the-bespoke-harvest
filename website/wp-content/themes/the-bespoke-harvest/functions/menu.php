<?php
function register_my_menu()
{
    register_nav_menu('header-menu', __('Header Menu'));
}

add_action('init', 'register_my_menu');

function clean_custom_menus()
{
    $menu_name = 'header-menu';
    if (($locations = get_nav_menu_locations()) && isset($locations[$menu_name])) {
        $menu = wp_get_nav_menu_object($locations[$menu_name]);
        $menu_items = wp_get_nav_menu_items($menu->term_id);
        $menu_list = do_shortcode('[tbhHeader items="' . encode_shortcode_data($menu_items) . '" cart="'.encode_shortcode_data(array()).'"]');

    } else {
        $menu_list = '<!-- no list defined -->';
    }
    echo $menu_list;
}
