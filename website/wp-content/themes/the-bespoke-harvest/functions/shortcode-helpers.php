<?php
function encode_shortcode_data($data)
{
    return base64_encode(json_encode($data));
}

function decode_shortcode_data($data)
{
    return json_decode(base64_decode($data));
}


function renderFirstFrame()
{
    if (have_rows('first_frame')): ?>
        <?php while (have_rows('first_frame')): the_row(); ?>
            <section class="section-scroller">
                <?= do_shortcode('[tbhFrame 
                                            images="' . encode_shortcode_data(get_sub_field('image')) . '" 
                                            first-line="' . encode_shortcode_data(get_sub_field('first_line')) . '" 
                                            second-line="' . encode_shortcode_data(get_sub_field('second_line')) . '"]')
                ?>
            </section>
        <?php endwhile;
    endif;
}


function renderWelcome()
{
    ?>
    <section class="section-scroller">
        <?= do_shortcode('[tbhWelcome 
                                    content="' . encode_shortcode_data(get_field('welcome_copy')) . '"]')
        ?>
    </section>
    <?php
}

function renderHeroes()
{
    if (have_rows('hero')): ?>
        <?php while (have_rows('hero')): the_row(); ?>
            <section class="section-scroller">
                <?= do_shortcode('[tbhHero
                                            images="' . encode_shortcode_data(get_sub_field('image')) . '" 
                                            first-line="' . encode_shortcode_data(get_sub_field('first_line')) . '" 
                                            second-line="' . encode_shortcode_data(get_sub_field('second_line')) . '"
                                            link="' . encode_shortcode_data(get_sub_field('link')) . '"]'
                )
                ?>
            </section>
        <?php endwhile;
    endif;
}

function renderApartments()
{
    $posts = postQueryToMapById(array('post_type' => 'apartment', 'post_status' => 'publish'));
    ?>
    <?php foreach ($posts as $post) : ?>
    <section class="section-scroller">
        <?= do_shortcode('[tbhHero
                                            images="' . encode_shortcode_data(get_field('image', $post->ID)) . '" 
                                            first-line="' . encode_shortcode_data($post->post_title) . '" 
                                            second-line="' . encode_shortcode_data(get_field('byline', $post->ID)) . '"
                                            link="' . encode_shortcode_data(get_field('url', $post->ID)) . '"]'
        )
        ?>
    </section>
<?php endforeach;

}

function renderDoubleColumn()
{
    if (have_rows('double_column')): ?>
        <?php while (have_rows('double_column')): the_row(); ?>
            <?= do_shortcode('[tbhDoubleColumn
                                            column-1="' . encode_shortcode_data(get_sub_field('column_1')) . '" 
                                            column-2="' . encode_shortcode_data(get_sub_field('column_2')) . '"]'
            )
            ?>
        <?php endwhile;
    endif;
}

function renderSingleColumn()
{
    if (have_rows('single_column')): ?>
        <?php while (have_rows('single_column')): the_row(); ?>
            <?= do_shortcode('[tbhSingleColumn
                                            column="' . encode_shortcode_data(get_sub_field('column')) . '"]'
            )
            ?>
        <?php endwhile;
    endif;
}