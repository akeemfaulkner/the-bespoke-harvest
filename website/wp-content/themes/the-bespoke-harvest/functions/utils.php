<?php

function postQueryToMapById($query)
{
    $result = new WP_Query($query);
    $posts = $result->posts;

    $tuples = array_map(function ($post) {
        return array($post->ID, $post);
    }, $posts);

    $result = array();
    foreach ($tuples as $value) {
        if (is_array($value))  $result[$value[0]] =  $value[1];
    }
    return $result;

}

function getCurrentURL(){
    global $wp;
    return  home_url(add_query_arg(array(),$wp->request));
}
