<?php

// load css into the website's front-end
function tbh_enqueue_style()
{
    wp_enqueue_style('tbh-style', get_stylesheet_uri());
    wp_enqueue_style('tbh-fa-style', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
    wp_enqueue_style('tbh-MAIN-style', get_template_directory_uri() . '/patterns/public/css/style.css', ['tbh-fa-style'], false);
}

add_action('wp_enqueue_scripts', 'tbh_enqueue_style');

// load css into the admin pages
function tbh_enqueue_options_style()
{
    $screen = get_current_screen();
    if ($screen->id === 'settings_page_my-orchard-admin') {
        wp_enqueue_style('tbh-options-style', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
        wp_enqueue_script('tbh-options-angular', 'https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js', array('jquery'), 1.0, true);
        wp_enqueue_script('tbh-options-index', get_template_directory_uri() . '/admin/index.js', array('tbh-options-angular'), 1.0, true);
    }
}

add_action('admin_enqueue_scripts', 'tbh_enqueue_options_style');


function tbh_enqueue_script()
{
    wp_enqueue_script('tbh-html-shim', get_template_directory_uri() . '/js/html5.js', array('jquery'), 1.0, true);
    wp_enqueue_script('tbh-product-proxy', get_template_directory_uri() . '/js/product-js.php', array(), 1.0);
    wp_enqueue_script('tbh-angular', 'https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js', array('jquery'), 1.0, true);
    wp_enqueue_script('tbh-js', get_template_directory_uri() . '/js/dist/bundle.js', array('jquery', 'tbh-html-shim', 'tbh-product-proxy'), 1.0, true);
}

add_action('wp_enqueue_scripts', 'tbh_enqueue_script');