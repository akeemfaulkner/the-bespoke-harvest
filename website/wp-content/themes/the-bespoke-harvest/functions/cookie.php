<?php
add_action('init', 'tbhStartCookie', 1);
function tbhStartCookie()
{
    $options = [
        'cost' => 12,
    ];

    if (empty($_COOKIE['uid'])) {
        $uid = uniqid() . $_SERVER['REMOTE_ADDR'];
        $one_week = time() + 7 * 24 * 60 * 60;
        setcookie('uid', password_hash($uid, PASSWORD_BCRYPT, $options), $one_week);
    }

}

