<?php
/* Template Name: Checkout */
get_header(); ?>
<?php
$posts = [];

$posts = get_posts(array(
    'numberposts' => -1,
    'post_type' => 'any',
    'meta_query' => array(
        array(
            'key' => 'sku',
            'compare' => 'EXISTS'
        ),
    )
));

$products = array();
foreach ($posts as $post) {
    $ID = $post->ID;
    $products[get_field('sku', $ID)] = array(
        'name' => $post->post_title,
        'url' => get_permalink($ID)
    );
}
?>

<section class="section-scroller" ng-app="shopping-cart">
    <div class="content" ng-controller="mainCtrl">

        <div class="content__header">
            <div class="serif-header">Shopping Basket</div>
            <br>
            <div class="serif-header" ng-if="isEmptyCart">Your Shopping Basket is empty.</div>
        </div>
        <div ng-if="!isEmptyCart">
            <table class="checkout-table">
                <thead class="checkout-table__head">
                <tr>
                    <th>
                        Sku
                    </th>
                    <th>
                        Name
                    </th>
                    <th>
                        Qty
                    </th>
                    <th>
                        Remove
                    </th>
                </tr>
                </thead>
                <tbody class="checkout-table__body">
                <tr ng-repeat="(sku,count) in cart">
                    <td>
                        {{sku}}
                    </td>
                    <td>
                        <a href="{{products[sku].url}}">{{products[sku].name}}</a>
                    </td>
                    <td>
                        <form method="post" action="">
                            <select ng-init="productCount = getOptionArray(count); initialAmount = cart[sku];"
                                    ng-model="cart[sku]"
                                    ng-change="onSelectChange($event, sku)"
                                    convert-to-number>
                                <option value="{{amount}}" ng-repeat="amount in productCount">{{amount}}</option>
                            </select>
                        </form>
                    </td>
                    <td>
                        <form method="post" action="">
                            <button class="hollow-button" value="{{sku}}" type="submit"
                                    name="<?= ShoppingCart::$Options['removeFromCart'] ?>"
                                    ng-click="removeFromCart($event, sku)">Remove
                            </button>
                        </form>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>

<script>
    var __PRODUCT_DATA__ = <?=json_encode($products); ?>;
</script>
<?php get_footer(); ?>

