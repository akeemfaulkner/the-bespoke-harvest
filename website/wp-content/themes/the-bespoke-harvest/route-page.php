<?php
/* Template Name: Route */

get_header(); ?>
<?php renderFirstFrame(); ?>

<?php renderWelcome(); ?>
<?= do_shortcode('[tbhProductList]') ?>
<section class="section-scroller map-section">
    <div id="map"></div>

    <div class="route-info route-info--visible">
        <div class="route-content-top">
            <div class="route-info__name map__name">Route1 KM 35</div>
            <div class="route-info__row">
                <div class="route-info__title">Start</div>
                <div class="route-info__content--inline map__start"></div>
            </div>
            <div class="route-info__row">
                <div class="route-info__title">Finish</div>
                <div class="route-info__content--inline map__finished"></div>
            </div>
            <div class="route-info__row route-info__description">
                <div class="route-info__title">Description</div>
                <div class="route-info__content map__description"></div>
            </div>
        </div>


        <div class="route-content-bottom map__methods">
            <div class="route-info__row">
                <div class="route-info__title">Cycling</div>
            </div>
            <div class="route-info__row">
                <div class="route-info__title">Hiking</div>
            </div>
            <div class="route-info__row">
                <div class="route-info__title">Sailing</div>
            </div>
        </div>
    </div>

    <div class="tabs">
        <?php $tabIndex = 0 ?>
        <?php if (have_rows('routes')): ?>
            <?php while (have_rows('routes')): the_row(); ?>
                <?php
                $from = get_sub_field('from');
                $to = get_sub_field('to');
                $description = get_sub_field('description');
                $name = get_sub_field('name');
                $traveling_methods = get_sub_field('traveling_methods');
                ?>
                <div class="tab map-tab"
                     data-tab="<?= $tabIndex ?>"
                     data-methods="<?= htmlspecialchars(json_encode($traveling_methods)) ?>"
                     data-name="<?= htmlspecialchars(json_encode($name)) ?>"
                     data-description="<?= htmlspecialchars(json_encode($description)) ?>"
                     data-from="<?= htmlspecialchars(json_encode($from)) ?>"
                     data-to="<?= htmlspecialchars(json_encode($to)) ?>">
                    <?= get_sub_field('name') ?>
                </div>
                <?php $tabIndex++ ?>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>

    <ul class="grid-3-2">
        <?php $imageIndex = 0 ?>
        <?php if (have_rows('routes')): ?>
            <?php while (have_rows('routes')): the_row(); ?>
                <li class="grid-3-2__item">
                    <div class="image-square" data-tab="<?= $imageIndex ?>">
                        <img src="<?= get_sub_field('thumbnail') ?>" alt="">
                    </div>
                </li>
                <?php $imageIndex++ ?>
            <?php endwhile; ?>
        <?php endif; ?>
    </ul>

</section>
<?php renderHeroes(); ?>

<section class="section-scroller">
    <?= do_shortcode('[tbhInstagram]') ?>
</section>
<?php get_footer(); ?>

