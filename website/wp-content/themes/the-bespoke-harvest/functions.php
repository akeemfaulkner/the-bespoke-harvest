<?php

foreach (glob(get_template_directory() . "/functions/*.php") as $filename) {
    include_once $filename;
}

foreach (glob(get_template_directory() . "/components/*.php") as $filename) {
    include_once $filename;
}

