<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<?php get_header(); ?>
<?php renderFirstFrame(); ?>
<?php renderWelcome(); ?>
<section class="section-scroller">
    <?= do_shortcode('[tbhOrchard]') ?>
</section>
<?php renderHeroes(); ?>
<section class="section-scroller">
    <?= do_shortcode('[tbhInstagram]') ?>
</section>

<?php get_footer(); ?>
