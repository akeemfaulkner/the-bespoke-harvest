import {isMobile} from "./utils";

$(function () {
    if(!isMobile()) {
        $.scrollify({
            section: ".section-scroller",
            scrollSpeed: 600,
            easing: 'linear',
            updateHash: false,
            setHeights: false,
            afterResize: function () {},
        });
    }
});