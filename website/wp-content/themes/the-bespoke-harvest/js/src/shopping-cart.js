import {cartService} from "./products";
const App = angular.module('shopping-cart', []);


App.directive('convertToNumber', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (val) {
                return val !== null ? parseInt(val, 10) : null;
            });
            ngModel.$formatters.push(function (val) {
                return val !== null ? '' + val : null;
            });
        }
    };
});

App.controller('mainCtrl', mainCtrl);

mainCtrl.$inject = ['$scope'];

function mainCtrl($scope) {
    $scope.products = __PRODUCT_DATA__;
    $scope.cart = {};
    $scope.selectedCount = {};

    const setCartData = (data)=>$scope.cart = data;

    cartService.addListener(cartService.FieldNames.getCart, setCartData);
    cartService.addListener(cartService.FieldNames.removeFromCart, setCartData);
    cartService.addListener(cartService.FieldNames.setItemAmount, setCartData);

    cartService.getCart();

    $scope.onSelectChange = function ($event, sku) {
        cartService.setItemAmount(sku, $scope.cart[sku]);
    };

    $scope.removeFromCart = function ($event, sku) {
        $event.preventDefault();
        cartService.removeFromCart(sku);
    };

    $scope.isEmptyCart = Object.keys($scope.cart).length === 0;

    $scope.getOptionArray = function (count) {
        return fillArray(count, i => count - i);
    };

}

function fillArray(count, cb) {
    let array = [];
    for (let i = 0; i <= count; i++) {
        array[i] = cb(i, array);
    }
    return array;
}
