import {isMobile} from "./utils";
import {NavHeight, ScreenSize} from "./config";

$(() => {

    const navFixedClass = 'nav--fixed';
    const navActiveClassName = 'nav--active';
    const $toggleButton = $('.nav-toggle');
    const $window = $(window);
    const $nav = $('.nav');

    if (!$nav) return;


    let isFixed = false;

    $toggleButton.click((event) => {
        isFixed = true;
        event.preventDefault();
        $nav.toggleClass(navActiveClassName)
    });

    $window.scroll(onScroll);
    onScroll();

    $window.resize(() => {
        if ($window.width() <= ScreenSize.small
            && isFixed
            && $nav.hasClass(navActiveClassName)) {

            $nav.removeClass(navFixedClass);
            $nav.removeClass(navActiveClassName);
            isFixed = false;
        } else {
            onScroll();
        }
    });

    function onScroll() {

        if (!isMobile()
            && $window.scrollTop() >= NavHeight
            && $window.width() >= ScreenSize.small) {

            $nav.addClass(navFixedClass);

            if (!$nav.hasClass(navActiveClassName) && !isFixed) {
                $nav.addClass(navActiveClassName);
                isFixed = true;
            }

        } else {

            $nav.removeClass(navFixedClass);
            isFixed = false;

        }
    }
});
