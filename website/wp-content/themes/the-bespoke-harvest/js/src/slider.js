import {getSlider} from 'simple-slider';


$(function () {
    const $els = $('[data-simple-slider]');

    if (!$els.length) return;
    let isReverse = false;

    $els.each((index, el) => {

        const $parentEl = $(el).parent('.hero');
        const $controls = $parentEl.find('.hero-controls');
        const slider = getSlider({
            paused: true,
            container: el,
            transitionTime: 1,
            delay: 4,
            prop: 'left',
            init: 100,
            show: 0,
            end: -100,
            unit: '%',
        });

        if ($controls) {
            const $leftControl = $parentEl.find('.hero-controls__left');
            const $rightControl = $parentEl.find('.hero-controls__right');
            $leftControl.on('click', onClickControl(true));
            $rightControl.on('click', onClickControl(false));
        }

        function onClickControl(reverse, callback) {
            return (e) => {
                if (reverse && !isReverse) {
                    isReverse = true;
                    slider.reverse();
                }

                if(!reverse && isReverse) {
                    isReverse = false;
                    slider.reverse();
                }

                slider.next();
                e.preventDefault();
                slider.pause();
            };
        }

    });
});

