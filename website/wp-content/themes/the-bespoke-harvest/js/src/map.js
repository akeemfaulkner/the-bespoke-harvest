// https://github.com/lukehaas/Scrollify/blob/master/jquery.scrollify.min.js


new class Map {

    constructor() {
        this.initialize = this.initialize.bind(this);
        this.$mapContainer = document.getElementById("map");
        if (!this.$mapContainer) return;
        this.$routeInfo = $('.route-info');
        this.$tabs = $('[data-tab]');
        this.$nameInfo = $('.map__name');
        this.$startInfo = $('.map__start');
        this.$finishedInfo = $('.map__finished');
        this.$descriptionInfo = $('.map__description');
        this.$methodsInfo = $('.map__methods');
        this.currentTabIndex_ = 0;

        Array.from(this.$tabs).forEach(($tab) => {
            const index = $($tab).data('tab');
            $tab.addEventListener('click', () => {
                if (this.currentTabIndex_ === index) return;
                this.loadTabData(index);
            });
        });

        google.maps.event.addDomListener(window, 'load', () => {
            this.loadTabData(0);
        });

    }


    initialize() {
        this.createMap();
        this.initializeMapLayer();
        this.createMarker(this.start);
        this.createLabel(this.start, 'A', 0, -.5);
        this.createMarker(this.end);
        this.createLabel(this.end, 'B', 0, .5);
        this.drawDirections();
        this.mapNameLabel = this.createLabel(this.start, this.nameData, 2, -1);
    }

    loadTabData(index) {
        const $tab = this.$tabs.eq(index);
        if (!$tab) return {};
        this.currentTabIndex_ = index;
        this.nameData = JSON.parse($tab.data('name'));
        this.fromData = $tab.data('from');
        this.toData = $tab.data('to');
        this.methodsData = $tab.data('methods');
        this.descriptionData = JSON.parse($tab.data('description'));
        this.end = new google.maps.LatLng(this.toData.lat, this.toData.lng);
        this.start = new google.maps.LatLng(this.fromData.lat, this.fromData.lng);

        if (this.mapNameLabel) {
            this.mapNameLabel.set('labelContent', this.nameData);
        }
        this.$nameInfo.html(this.nameData);
        this.$startInfo.html(this.fromData.address);
        this.$finishedInfo.html(this.toData.address);
        this.$descriptionInfo.html(this.descriptionData);
        this.renderMethods();
        this.initialize();
        this.map.setCenter(this.start);
    }

    createMap() {
        this.map = new google.maps.Map(this.$mapContainer, {
            center: this.start,
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true,
            styles: this.getStyles(),
        });

        google.maps.event.addDomListener(window, 'resize', () => {
            this.map.setCenter(this.start);
        });

        this.addClickeventListener(this.map);
    }

    addClickeventListener(el) {
        el.addListener('click', () => {
            this.$routeInfo.toggleClass('route-info--visible');
        });
    }

    drawDirections() {
        const directionsService = new google.maps.DirectionsService();
        const request = {
            origin: this.start,
            destination: this.end,
            travelMode: google.maps.TravelMode.DRIVING
        };

        directionsService.route(request, (result, status) => {
            if (status === google.maps.DirectionsStatus.OK) {
                this.paintPolyline(this.map, result.routes[0].overview_path);
            }
        });
    }

    createMarker(position) {
        const marker = new google.maps.Marker({
            icon: this.getMarkerConfig(),
            position: position
        });
        marker.setMap(this.map);
        return marker;
    }

    createLabel(position, label, offsetY = 0, offsetX = 0) {
        const marker = new google.maps.Marker({
            icon: this.getMarkerConfig({
                fillColor: 'transparent',
                strokeColor: 'transparent',
                scale: 40,
                anchor: new google.maps.Point(offsetX, offsetY)
            }),
            label: {text: label, color: "white", fontSize: '20px'},
            position: position
        });
        marker.setMap(this.map);
        return marker;
    }

    initializeMapLayer() {
        this.layer = new google.maps.FusionTablesLayer({
            query: {
                select: '\'Id\'',
                from: '1ooVyXMEnEBkDaKAuzTypdFisXGZB87-n-hRwUWM'

            },
            styles: [{
                polygonOptions: {
                    fillColor: '#e1e1e1',
                    fillOpacity: 1.0
                }
            }, {
                where: 'Id = \'Italy\'',
                polygonOptions: {
                    fillColor: '#959595',
                    fillOpacity: 1.0
                }
            }],
            options: {
                suppressInfoWindows: true
            }
        });
        this.addClickeventListener(this.layer);
        this.layer.setMap(this.map);
        return this.layer;
    }

    getMarkerConfig(config = {}) {
        return Object.assign({
            path: google.maps.SymbolPath.CIRCLE,
            fillOpacity: 1.0,
            fillColor: "white",
            strokeOpacity: 1.0,
            strokeColor: "white",
            strokeWeight: 1.0,
            scale: 5.0
        }, config);
    }

    paintPolyline(map, pathCoords) {
        let i, route;

        route = new google.maps.Polyline({
            path: [],
            geodesic: true,
            strokeColor: '#fac005',
            strokeOpacity: 1.0,
            strokeWeight: 4,
            editable: false,
            map: map
        });


        for (i = 0; i < pathCoords.length; i++) {
            setTimeout(function (coords) {
                route.getPath().push(coords);
            }, 5 * i, pathCoords[i]);
        }
    }

    getStyles() {
        return [
            {
                featureType: "water",
                elementType: "all",
                stylers: [
                    {color: '#e1e1e1'},
                    {visibility: "on"}
                ]
            }, {
                featureType: "road",
                stylers: [
                    {"visibility": "off"}
                ]
            }, {
                featureType: "transit",
                stylers: [
                    {"visibility": "off"}
                ]
            }, {
                featureType: "administrative",
                stylers: [
                    {"visibility": "off"}
                ]
            }, {
                featureType: "landscape",
                elementType: "all",
                stylers: [
                    {"color": "#e1e1e1"}
                ]
            }, {
                featureType: "poi",
                //elementType: "labels",
                stylers: [{
                    visibility: "off"
                }]
            },
            {
                elementType: "labels",
                stylers: [
                    {"visibility": "off"}
                ]
            }
        ];
    }


    renderMethods() {
        const $div = $('<div />');
        this.$methodsInfo.html('');
        if (this.methodsData instanceof Array) {
            this.methodsData.forEach((method) => {
                let html = $div.html();
                $div.html(html + `
                <div class="route-info__row">
                    <div class="route-info__title">${method}</div>
                </div>
             `);
            });
        }

        this.$methodsInfo.html($div.html());
    }
};