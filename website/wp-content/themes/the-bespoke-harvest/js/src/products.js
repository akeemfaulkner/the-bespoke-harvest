class CartService {

    constructor() {

        this.FieldNames = window.__TBH__.FieldNames;

        this.__listenerMap__ = {
            [this.FieldNames.getCart]: [],
            [this.FieldNames.addToCart]: [],
            [this.FieldNames.removeFromCart]: [],
            [this.FieldNames.bookNow]: [],
            [this.FieldNames.setItemAmount]: []
        };

    }

    /**
     *
     * @param {String} eventType
     * @param {Function} callback
     * @param {object} ctx
     */
    addListener(eventType, callback, ctx = null) {
        let listener = this.__listenerMap__[eventType];
        listener && this.__listenerMap__[eventType].push({callback, ctx});
    }

    /**
     *
     * @param {String} eventType
     * @param {Function} callback
     * @param {object} ctx
     */
    removeListener(eventType, callback, ctx) {
        let listener = this.__listenerMap__[eventType];
        if (!listener) return;
        this.__listenerMap__[eventType] = this.__listenerMap__[eventType].filter(listener => {
            return listener.callback !== callback && listener.ctx !== ctx;
        });
    }

    publishEvent(eventType, payload) {
        let listener = this.__listenerMap__[eventType];
        if (!listener) return;
        this.__listenerMap__[eventType].forEach(listener => {
            listener.callback.call(listener.ctx, payload);
        });
    }

    getStorage_() {
        let cart = window.sessionStorage.getItem('tbhCart');
        return cart ? JSON.parse(cart) : {};
    }

    setStorage_(data) {
        window.sessionStorage.setItem('tbhCart', JSON.stringify(data));
    }

    getCart() {
        this.handleGetCart_(this.getStorage_());
    }

    addToCart(sku) {
        const cart = this.getStorage_();
        const item = cart[sku];
        cart[sku] = item ? item + 1 : 1;
        this.setStorage_(cart);
        this.handleAddToCart_(cart);
    }

    bookNow(sku) {
        const cart = this.getStorage_();
        const item = cart[sku];
        cart[sku] = item ? item + 1 : 1;
        this.setStorage_(cart);
        this.handleBookNow_(cart);
    }

    removeFromCart(sku) {
        const cart = this.getStorage_();
        delete  cart[sku];
        this.setStorage_(cart);
        this.handleRemoveFromCart_(cart);
    }

    setItemAmount(sku, amount) {
        const cart = this.getStorage_();
        if (amount > 0) {
            cart[sku] = amount;
        } else {
            delete  cart[sku];
        }

        this.setStorage_(cart);
        this.handleSetItemAmount_(cart);
    }

    handleGetCart_(payload) {
        this.publishEvent(this.FieldNames.getCart, payload);
    }

    handleAddToCart_(payload) {
        this.publishEvent(this.FieldNames.addToCart, payload);
    }

    handleRemoveFromCart_(payload) {
        this.publishEvent(this.FieldNames.removeFromCart, payload);
    }

    handleBookNow_(payload) {
        this.publishEvent(this.FieldNames.bookNow, payload);
    }

    handleSetItemAmount_(payload) {
        this.publishEvent(this.FieldNames.setItemAmount, payload);
    }

}

export const cartService = new CartService();

$(function () {
    cartService.getCart();
    const $addToCart = getButtonByName(cartService.FieldNames.addToCart);
    attachListener($addToCart, 'click', function (e) {
        e.preventDefault();
        const $this = $(this);
        const sku = $this.val();
        cartService.addToCart(sku);
    });

    const $bookNow = getButtonByName(cartService.FieldNames.bookNow);
    attachListener($bookNow, 'click', function (e) {
        e.preventDefault();
        const $this = $(this);
        const sku = $this.val();
        cartService.bookNow(sku);
    });

    const $removeFromCart = getButtonByName(cartService.FieldNames.removeFromCart);
    attachListener($removeFromCart, 'click', function (e) {
        e.preventDefault();
        const $this = $(this);
        const sku = $this.val();
        cartService.removeFromCart(sku);
    });

});
const setSmallCartHTML = function (data) {
    let count = 0;
    for (let i in data) count += parseInt(data[i], 10);
    $('.nav__cart-count').html(count);
};

cartService.addListener(cartService.FieldNames.addToCart, setSmallCartHTML);
cartService.addListener(cartService.FieldNames.getCart, setSmallCartHTML);
cartService.addListener(cartService.FieldNames.removeFromCart, setSmallCartHTML);
cartService.addListener(cartService.FieldNames.setItemAmount, setSmallCartHTML);
cartService.addListener(cartService.FieldNames.bookNow, function () {
    window.location.href = window.location.origin + '/checkout/';
});


function getButtonByName(name) {
    return $('[name="' + name + '"]');
}

function attachListener($element, eventName, listener) {
    if ($element && $element.length) {
        $element.on(eventName, listener);
    }
}
