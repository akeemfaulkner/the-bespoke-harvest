window.$ = window.jQuery;
import 'jquery-scrollify';

export const ScreenSize = {
    xsmall: 400,
    small: 761,
    medium: 960
};

export const NavHeight = 134;