<?php

require_once(__DIR__ . "/../functions/ShoppingCart.php");
header('Content-Type: application/javascript');

?>

<?php
$fieldNames = json_encode(ShoppingCart::$Options, JSON_PRETTY_PRINT);
$javascript = /** @lang javascript */ <<<EOT
window.__TBH__ = {};
window.__TBH__.FieldNames = {$fieldNames}
EOT;

echo $javascript;
?>