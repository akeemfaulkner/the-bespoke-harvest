FROM php:7.0-apache
COPY website/ /var/www/html/
COPY cartSocket.conf /etc/init/cartSocket.conf

WORKDIR /var/www/html/

EXPOSE 80

RUN pecl install redis-3.1.0 \
	&& pecl install xdebug-2.5.0 \
	&& docker-php-ext-enable redis xdebug

RUN docker-php-ext-install mysqli pdo pdo_mysql
RUN a2enmod rewrite
